package com.example.myproject.todo;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TodoItemTest {

    private TodoItem testItem;

    @Before
    public void setUp() throws Exception {
        testItem = new TodoItem(1, "Pass the test");
    }

    @Test
    public void testGetterMethods() throws Exception {
        assertEquals(1, testItem.getId().intValue());
        assertEquals("Pass the test", testItem.getTitle());
    }

    @Test
    public void testSetterMethods() throws Exception {
        testItem.setId(2);
        testItem.setTitle("Pass the bread");

        assertEquals(2, testItem.getId().intValue());
        assertEquals("Pass the bread", testItem.getTitle());
    }

    @Test
    public void testStringRepresentation() throws Exception {
        final String expected = "TodoItem{id=1, title='Pass the test'}";

        assertEquals(expected, testItem.toString());
    }
}