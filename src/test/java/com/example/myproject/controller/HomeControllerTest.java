package com.example.myproject.controller;

import com.example.myproject.service.TodoService;
import com.example.myproject.todo.TodoItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(HomeController.class)
public class HomeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TodoService todoService;

    @Test
    public void testIndexPage() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("home"));
    }

    @Test
    public void testIndexPageHasTodoItems() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(model().attributeExists("items"));
    }

    @Test
    public void testNewTodoShouldRedirectToIndex() throws Exception {
        mockMvc.perform(post("/")
                .param("item_text", "Pass the test"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void testNewTodoSaved() throws Exception {
        doCallRealMethod().when(todoService).add(any(TodoItem.class));
        when(todoService.getAll()).thenCallRealMethod();

        mockMvc.perform(post("/")
                .param("item_text", "Pass the test")).andReturn();

        assertThat(todoService.getAll().size()).isEqualTo(1);
    }
}
