package com.example.myproject.controller;

import com.example.myproject.service.TodoService;
import com.example.myproject.todo.TodoItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @Autowired
    private TodoService todoService;

    @GetMapping("/")
    public String viewHomePage(Model model) {
        model.addAttribute("items", todoService.getAll());
        return "home";
    }

    @PostMapping("/")
    public String postNewTodo(@RequestParam("item_text") String title) {
        todoService.add(new TodoItem(0, title));
        return "redirect:/";
    }
}
