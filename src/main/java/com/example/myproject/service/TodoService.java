package com.example.myproject.service;

import com.example.myproject.todo.TodoItem;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TodoService {

    private static final List<TodoItem> ITEMS = new ArrayList<>();

    public List<TodoItem> getAll() {
        return ITEMS;
    }

    public void add(TodoItem item) {
        ITEMS.add(item);
    }
}
