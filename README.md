# Spring CI Tutorial

> Spring Boot application example for demonstrating GitLab CI/CD pipeline.

[![pipeline status](https://gitlab.com/addianto/spring-ci-tutorial/badges/master/pipeline.svg)](https://gitlab.com/addianto/spring-ci-tutorial/commits/master)
[![coverage report](https://gitlab.com/addianto/spring-ci-tutorial/badges/master/coverage.svg)](https://gitlab.com/addianto/spring-ci-tutorial/commits/master)

## Getting Started

Contoh aplikasi Web ini merupakan artefak kode yang digunakan dalam sesi
penyuluhan Test-Driven Development (TDD) dan Continuous Integration (CI)
pada tanggal 19 Oktober 2018 di Fakultas Ilmu Komputer Universitas Indonesia.

Kebutuhan-kebutuhan minimal untuk dapat menjalankan dan mengembangkan contoh
ini lebih lanjut adalah sebagai berikut:

- Java JDK (v11)
- Maven (v3.5.3)
- Teks editor atau IDE favorit Anda

Daftar *library* beserta versi yang digunakan dapat dilihat dalam berkas
[pom.xml](pom.xml).

Untuk menjalankan *test suite* aplikasi Web ini secara lokal:

```bash
mvn clean  # opsional
mvn test
```

Untuk menjalankan aplikasi Web ini secara lokal:

```bash
mvn clean  # opsional
mvn spring-boot:run  # berjalan di localhost:8080
```

*Deployment* dilakukan secara otomatis oleh server GitLab CI ke Heroku. Untuk
melihat aplikasi Web yang telah di-*deploy* berdasarkan *commit* terakhir di
*branch* `master`, silakan buka tautan berikut: https://safe-refuge-70687.herokuapp.com/

## Features

Berikut ini adalah fitur-fitur yang terdapat dalam contoh aplikasi Web ini:

- Daftar tugas yang sangat minimalis
- Tata letak halaman dan *styling* menggunakan
[Bootstrap 4.1.3](https://getbootstrap.com/docs/4.1/) melalui mekanisme
[WebJars](https://www.webjars.org/)

Dari segi praktik konstruksi perangkat lunak, contoh aplikasi Web ini telah
menerapkan:

- Test-Driven Development
    - Unit testing
    - Mock testing
- Continuous Integration & Continuous Deployment
- Git Workflow (Feature Branch)
- Dependency Injection

## Introduction to TDD

Contoh aplikasi Web ini dikembangkan dengan mengikuti metodologi Test-Driven
Development (TDD). TDD menekankan para pengembang untuk memiliki *test*
terlebih dahulu sebelum mengimplementasikan sebuah fitur. Ketika *test* dan
implementasi sudah dibuat, maka selanjutnya adalah melakukan *refactoring*
untuk memperbaiki desain kode tanpa merusak implementasinya. Siklus ini
dilakukan secara iteratif hingga semua fitur memiliki *test* dan implementasi.

Salah satu kesalahpahaman yang sering terjadi ketika mempelajari TDD adalah
membuat **semua _test_ yang mungkin** terlebih dahulu, kemudian baru membuat
implementasi untuk meluluskan semua *test*. Hal ini tidak disarankan karena
akan memperlambat siklus *feedback* antara pengembang dengan kode yang
dikerjakan. Bayangkan Anda diminta mengerjakan PR sebanyak 100 soal dan Anda
baru mendapatkan nilai akhirnya setelah mengerjakan seluruh soal. Mungkin
tidak akan masalah apabila hasil Anda bagus dan benar semua. Namun apabila
terdapat banyak kesalahan setelah Anda kerjakan sekian lama, kemungkinan
Anda akan merasa kehilangan motivasi.

Melanjutkan analogi PR 100 soal di atas, bayangkan apabila setiap kali
mengerjakan 1 soal, Anda langsung mendapatkan *feedback* benar/salahnya
jawaban Anda. Apabila jawaban Anda benar, Anda akan merasa senang dan merasa
ada sesuatu yang berhasil dicapai. Apabila jawaban Anda tidak tepat, Anda
setidaknya masih memiliki motivasi untuk memperbaiki dan melakukan revisi
karena Anda belum terlanjur menginvestasikan banyak waktu Anda untuk
mengerjakan seluruh soal.

Dalam contoh aplikasi Web ini, TDD diterapkan dalam proses implementasi
fitur daftar tugas (*To-Do*) terbuka yang dapat diakses secara *online*.
Halaman daftar tugas dapat diakses dengan membuka halaman *index* aplikasi
Web-nya. Di dalam halaman daftar tugas, akan ada sebuah *form* untuk
menambahkan tugas baru. Apabila sudah ada 1 atau lebih tugas, maka halaman
yang sama akan mengandung daftar tugas.

Berikut ini adalah langkah-langkah TDD yang diikuti untuk menyelesaikan
fitur daftar tugas:

1. Buat sebuah *test* yang memastikan ketika *browser* membuka halaman
index, ada sebuah halaman Web yang dikembalikan ke *client*
    - *Test* dapat dibuat sebagai *functional test* (TODO) atau
    *mock test* yang menyimulasikan komunikasi *request*-*response*
    antara *client* dengan aplikasi Web
2. Implementasi sebuah `Controller` yang akan mengirimkan *response* berupa
halaman kosong (atau "Hello, World" sederhana) ketika ada *request*
halaman index
3. Buat sebuah *test* yang memastikan bahwa dalam halaman index yang
dikembalikan ke *client*, ada sebuah *form* masukan untuk membuat
tugas baru
    - Seperti *test* untuk nomor (1), lakukan pengecekan bahwa *response*
    mengandung elemen HTML *form*
4. Implementasi pembuatan halaman HTML di fungsi yang mengembalikan
*response* dalam `Controller` yang dibuat pada nomor (2)
5. Buat sebuah *test* yang memastikan bahwa ketika pengguna membuat tugas
baru dan melakukan *submit*, maka aplikasi akan menyimpan tugas baru
tersebut dan menampilkannya kembali kepada pengguna
    - Apa saja *test* yang harus dibuat?
    - Test 1: Pastikan *client* mengirimkan data terkait tugas baru sesuai
    dengan format yang diharapkan oleh aplikasi (tepatnya: format yang
    diharapkan oleh `Controller`)
    - Test 2: Pastikan tugas baru tersimpan di dalam aplikasi (misal: cek
    jumlah elemen di *database*)
    - Test 3: Pastikan halaman yang dikembalikan mengandung tugas baru
6. Implementasi penyimpanan tugas baru secara bertahap dengan berusaha
meluluskan satu *test* terlebih dahulu

## Maintainers

- [Daya Adianto](https://gitlab.com/addianto)

## License

[Apache License 2.0](LICENSE)
